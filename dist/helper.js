isNotEmpty = function(name,value) {
  $('#'+name).remove();
   if(value) 
    return true;
   else {
    var input = $("input[name="+name+"]");
    var placeholder =  input.attr("placeholder");
    input.after("<div id='"+name+"' class='error'>"+placeholder+" is required.</div>")
    return false;
   } 
}

isSelected = function(name,value) {
   $('#'+name).remove();
   if (value && value !== '') {
        return true;
    }
    else {
    var input = $("select[name="+name+"]");
    input.after("<div class='error'> Please select "+name+".</div>")
    return false;
   }  
}

isChecked = function(name) {
    $('#'+name).remove();
    if ($('[name="'+name+'"]').is(':checked')) {
        return true;
    } else {
        var input = $("input[name="+name+"]");
        input.after("<div class='error'> Please select "+name+".</div>")
        return false;
    }
};

isNumber = function(name,value) {
    $('#'+name).remove();
    var numberFilter = /^[0-9]+$/;
    if (numberFilter.test(value)) {
        return true;
    } else {
      var input = $("input[name="+name+"]");
      var placeholder =  input.attr("placeholder");
      input.after("<div id='"+name+"' class='error'>"+placeholder+" should be numeric.</div>")
      return false;
   }
}

isEmail = function(name,value) {
    $('#'+name).remove();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(value)) {
        return true;
    }
    else {
        var input = $("input[name="+name+"]");
        input.after("<div id='"+name+"' class='error'> Please enter a valid email.</div>")
        return false;
    }
};

isPhoneNumber = function(name,value) {
    $('#'+name).remove();
    var filter = /^\(?(\d{3})\)?[ .-]?(\d{3})[ .-]?(\d{4})$/;
    if (filter.test(value)) {
        return true;
    }
    else {
        var input = $("input[name="+name+"]");
        input.after("<div id='"+name+"' class='error'> Please enter a valid phone number.</div>")
        return false;
    }
};