import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, useRouterHistory } from "react-router";
import { Provider } from 'react-redux'
import createHistory from 'history/lib/createBrowserHistory'
import store from './store.jsx'

import App from './App.jsx';
import Home from "./components/home.jsx";
import About from "./components/about.jsx";
import Contact from "./components/contact.jsx";
import Product from "./components/product.jsx";
import Settings from "./components/settings.jsx";
import Other from "./components/other.jsx";
import Viewcart from "./components/cart.jsx";
import Checkout from "./components/checkOut.jsx";
import OrderThanks from "./components/orderThanks.jsx";
import Category from "./components/category.jsx";
import Search from "./components/search.jsx";

const history = useRouterHistory(createHistory)({ basename: '/' })
class Main extends React.Component {
  render() {
    return (
      <Router history={history} >
        <Route name="app" component={App}>
          <Route name="home" path="/" component={Home} />
          <Route name="about" path="/about" component={About} />
          <Route name="product" path="/product/:productId" component={Product} />
          <Route name="contact" path="/contact" component={Contact} />
          <Route name="cart" path="/viewcart" component={Viewcart} />
          <Route name="checkout" path="/checkout" component={Checkout} />
          <Route name="orderThanks" path="thankyou/:orderId" component={OrderThanks} />
          <Route name="category" path="category/:category_code" component={Category} />
          <Route name="search" path="search/:query" component={Search} />
        </Route>
      </Router>
    )
  }
}

ReactDOM.render(<Provider store={store} ><Main /></Provider>, document.getElementById('app'));


