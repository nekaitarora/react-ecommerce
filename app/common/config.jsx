let config  = {
  "baseApiUrl" : "http://192.168.100.172:3000/",
  "clientUrl" : "http://192.168.100.172:4000",
  "googleApiKey" : "AIzaSyD8LStFEtLQHWd-3TI84inZhyE7FKUlCCM",
  "store_code" : "Mi-Casa41",
  "store_Name" : "Mi Casa",
  "currency" : "$",
  "shipment_message" : "Each store ships products independently and each store is responsible to make sure you receive the merchandise in the time specified. 'The Mall' will work with each store to make sure the quality of the products and the service provided is excellent. Immediately after placing your order you will see the number of shipments estimates and costs associated with them.",
  "notification_message":"Messages related to the orders will be sent to the email address associated with the billing address.  If you wish to be notified about delivery times and progress of your order using an additional email address or via instant messages, these will be sent to the email address and/or the mobile phone specified in this section. Please note that your phone service provider may charge for the messages sent to your mobile phone.",
  "payment_message": "Credit Card information is never stored in the Mall's or any Store's computers in a highly secure payment entity. The information is encrypted and follows all the regulations (PCI-DSS Level 1) with the highest levels of security",
  "Month_Array" : ['1','2','3','4','5','6','7','8','9','10','11','12'],
  "Year_Array" : ['2015','2016','2017','2018','2019','2020','2021','2022','2023','2024','2025'
                           ,'2026','2027','2028','2029','2030'],
  toFixed(argument) {
	  if(!argument)
      argument = 0;
    return Number(argument).toFixed(2);
  },
  "PublishableStripeKey" : "pk_test_gdu2IJDVOOjUTokNQDm7urqq"
}

module.exports = config;
