/* API Request */
// import config from '../../config/test.js';

// const requestHeader = {
//     'appId': config['appId'],
//     'appSecret': config['appSecret']
// }

export default new class API {
  constructor(){}

  // GET Type Request
  get(url, data, token, cb) {
    _requestToAPI('GET', url, data, token, cb);
  }

  // POST Type Request
  post(url, data, token, cb) {
    _requestToAPI('POST', url, data, token, cb);
  }

  // PUT Type Request
  put(url, data, token, cb) {
    _requestToAPI('PUT', url, data, token, cb);
  }

  // DELETE Type Request
  delete(url, data, token, cb) {
    _requestToAPI('DELETE', url, data, token, cb);
  }
};

//
// JQuery.ajax() request to Configuration target Server
// With `Authorization` header for server authentication
//
function _requestToAPI(type, url, data, cb) {
  if ($.isFunction(data)) {
    cb = data;
    data = null;
  } else if (!$.isFunction(cb)) {
    cb = function(){};
  }

  let ajaxOption = {
     type : type,
     dataType: 'json',
     url: url.trim(),
     contentType: 'application/json',
     xhrFields: {
         withCredentials: true
     },
     crossDomain: true,
     // headers : requestHeader,
     success: (data) => { cb(null, data) },
     error: (xhr, status, error) => { cb(error, status, xhr); },
     timeout: 10000
  }

  if(data) ajaxOption['data'] = JSON.stringify(data);
  $.ajax(ajaxOption);
}