import React, { Component } from 'react';

export default class Message extends Component {
  render() {
      return (
          <div className={"message " + (this.props.user === this.props.loggedUser ? "askMessage" : (this.props.user === "BOT" ? "botMessage" : "replyMessage" ))}>
              <strong>{this.props.user}: </strong> 
              <span>{this.props.text}</span>        
          </div>
      );
  }
}