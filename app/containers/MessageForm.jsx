import React, { Component } from 'react';

export default class MessageForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text : ''
    };
  }

  handleSubmit(e) {
    e.preventDefault();
    var message = {
      user : this.props.user,
      text : this.state.text
    };
    this.props.onMessageSubmit(message); 
    this.setState({ text: '' });
  }

  changeHandler(e) {
    this.setState({ text : e.target.value });
  }

  render() {
    return(
      <div className='message_form'>
          <form onSubmit={(event) => {this.handleSubmit(event)}}>
            <input onChange={(event) => {this.changeHandler(event)}} value={this.state.text} />
          </form>
      </div>
    );
  }
}