import React, { Component } from 'react';
import { Link } from 'react-router';

import productActions from "../actions/home.jsx";
import productStore from "../reducers/home.jsx";
import conifg from "../common/config.jsx";

var scroll_in_process = false;
var i = 0;
var skip = 0;
var limit = 4;
var listener;

export default class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [],
      gettingProduct: false,
    };
  }

  componentWillMount () {
    this.setState({
      categories: this.props.data.categories
    });
    listener = this.handleScroll.bind(this)
    window.addEventListener('scroll', listener);
  }

  componentWillUnmount () {
      i = 0;
      skip = 0;
      limit = 4;
      scroll_in_process = false;
      window.removeEventListener('scroll', listener);
  }
  scrollTo(category_code) {
    const element = document.querySelector("#"+category_code);
    element.scrollIntoView();
  }
  handleScroll(event) {
    var self = this;
    if ($("#scrollDiv").offset() && ($(window).scrollTop() + $(window).height()) > $("#scrollDiv").offset().top && !scroll_in_process) {
      scroll_in_process = true;
      if (this.props.data.categories[i]) {
        productActions.getProducts({category_code: this.props.data.categories[0].category_code,limit: limit,skip: skip}, (err, data, more) => {
          if(data.length == 0){
            i = i + 1;
            if (self.props.data.categories[i] && self.props.data.categories[i].category_code) {
              skip = 0;
              productActions.getProducts({category_code: self.props.data.categories[i].category_code,limit: limit,skip: skip}, (err, data, more) => {
                var categories = self.state.categories;
                categories[i]["product"] = [];  
                for(var j=0;j<data.length;j++){
                  categories[i]["product"].push(data[j]);
                }
                self.setState({
                  categories : categories
                })
                skip = skip + limit;
                
                scroll_in_process = false;
              })
            } else {
              i = i - 1;
              scroll_in_process = false;
            }
          } else {
            var categories = self.state.categories;
            if (! categories[i]["product"]) {
                categories[i]["product"] = []; 
            }
            for(var j=0;j<data.length;j++){
              categories[i]["product"].push(data[j]);
            }
            self.setState({
                  categories : categories
            })
            skip = skip + limit;
            
            scroll_in_process = false;
          }
        }); 
      }       
    }
  }

  render() {
    var host = conifg.baseApiUrl;
    var store = this.props.data;
    var self = this;
    var categories = this.props.data.categories.map(function(category,index) {
      return (
        <li key={index}> <Link  key={category.category_code} className="menu_select" to={"/#"+category.category_code} onClick={() =>{self.scrollTo(category.category_code)}}><b >{category.name}</b></Link></li>
      );
    });

    var products = this.state.categories.map(function(category,index){
      if(category.product) {
        var odd = index%2;
        return (
        <div key={index}>  
          <div className={ !odd ? 'col-xs-12 padding_lg bg_1' : 'col-xs-12 padding_lg bg_2' }> 
            <div className="col-xs-12 f5" id={category.category_code}>{category.name}</div>
            <div className="col-xs-12 padding_lg">
              
                  <Products data={category.product} / >
             
            </div>
          </div>
        </div>
        )
      }
    })

    return (
      <div className="productList">
        
        <section>
          <div className="col-xs-12 bg_2 padding_lg" id="store_menu">
            <div className="col-xs-12 text-center none f5">{store.name}</div>
            <div className="col-xs-12">
              <nav className="navbar navbar-default navi" role="navigation">
                <div className="container-fluid padding">
                  <div className="navbar-header margin">
                    <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span className="sr-only">Toggle Navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    </button>
                  </div>
                  <div className="navbar-collapse collapse in small-nav menu margin none" id="bs-example-navbar-collapse-1">
                    <div className="">
                      <ul className="nav navbar-nav f10">
                        {categories}
                      </ul>
                    </div>
                  </div>
                </div>
              </nav>
            </div>
          </div>  
        </section>
        
        <section>
           
          {products}
         
        </section>
        <div id="scrollDiv">&nbsp;</div>
      </div>
    );
  }
}

class Products extends Component { 

render() {
    var host = conifg.baseApiUrl;
  var categoryProduct = this.props.data.map(function(product){
            if(product)
              return (
                    <div key={product.product_code} className="col-md-4 col-sm-6 col-xs-6 padding_sm">
                      <Link className="text_3" to={'product/'+product.product_code} >
                        <div className="col-xs-12 none border">{product.variations[0].Assets.imagelinks[0] ? (<img alt={product.variations[0].Assets.imagelinks[0].alt} src={host+product.variations[0].Assets.imagelinks[0].image} />) : null}</div>
                        <div className="col-xs-8 none limit">{product.name.length > 22 ? product.name+'...':product.name}</div>
                        <div className="text-right col-xs-4 none limit text_1"><b>{"$"+product.variations[0].Pricing.regularPrice}</b></div>
                      </Link> 
                    </div>   
              )
          
        })
  return (
    <span> 
      {categoryProduct}
    </span> 
    )
}
}