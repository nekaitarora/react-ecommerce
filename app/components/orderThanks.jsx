import React, { Component } from 'react';

import config from "../common/config.jsx";
import API from "../common/api.jsx";
import IfRender from './ifRender.jsx';

let baseAPIUrl = config.baseApiUrl;

export default class OrderThanks extends Component {
  constructor(props) {
    super(props);
    this.state = {
     orderdata : []
    };
  }
  componentWillMount() {
    var self = this;
    API.get(baseAPIUrl + "order/"+this.props.params.orderId, null, function(err,result,more) {
      self.setState({
        orderdata : result
      }) 
    });
  }


  render() {
    var orderdata = this.state.orderdata;
    var host = config.baseApiUrl;
    return (
      <form name="thankyouForm" novalidate> 
      { orderdata && orderdata.items && orderdata.items[0] ? (
       
        <div className="col-xs-12 none" id="thankyou">
             <div  className="col-xs-12 none text-center f5 padding_lg z_black"><b>Thank you for your Order!</b></div>

             <div  className="col-xs-12 none text-right margin_sm z_black">Order Number : {orderdata.ordernumber}</div>
           
             <div className="col-xs-12 none text-justify f7 padding_lg">Your order is being processed. Your items will be delivered in the shortest amount of time possible. Each Store will deliver its products independently. We will send you information regarding the status of your shipments and inform you of any unforeseen issues. If you have any questions please email us <a href=""> service@thismall.com </a> or call us at (800)555-1212 </div>
             <div className="col-xs-12 none">
                <div  className="col-md-4 col-sm-4 col-xs-12 none">
                   <div className="f5 z_black">Shipping Address</div>
                 
                
                   <div>{orderdata.items[0].shippingDetails.first_name} {orderdata.items[0].shippingDetails.last_name}</div>
                   <div>{orderdata.items[0].shippingDetails.phoneno}</div>
                   <div>{orderdata.items[0].shippingDetails.address_line1}</div>
                   <div>{orderdata.items[0].shippingDetails.address_line2}</div>
                   <div>{orderdata.items[0].shippingDetails.city}, {orderdata.items[0].shippingDetails.postalcode}</div>
                   
                </div>
                   {orderdata.items[0].billingDetails ? (
                    <div  className="col-md-4 col-sm-4 col-xs-12 none">
                       <div  className="f5 z_black">Billing Address</div>
                           <div>{orderdata.items[0].billingDetails.address_line1}</div>
                           <div>{orderdata.items[0].billingDetails.address_line2}</div>
                           <div>{orderdata.items[0].billingDetails.city}, {orderdata.items[0].billingDetails.postalcode}</div>
                       
                    </div>
                    ): null }
                <div className="col-md-4 col-sm-4 col-xs-12 none">
                   <div  className="f5 z_black">Payment Details</div>
                 
                   <div><img src="image/card.png" /></div>
                   <div> 
                      {orderdata.items[0].paymentmethods.card_name}
                   </div>
                   <div> 
                      {orderdata.items[0].paymentmethods.masked_number}
                   </div>
                </div>
             </div>
             <div>
            <div  className="col-xs-12 padding_lg f5 z_black">Order Details</div>

                 <div className="col-xs-12 padding_lg border_b none">
                  <div className="col-md-12 col-sm-9 col-xs-12 none b_none">
            
                { orderdata.items[0].store_items.map(function (order) {
                     return (
                       <div ng-repeat="order in storeitem.store_items" className="col-xs-12 border_b padding_md">
                         <div className="col-md-2 col-sm-4 col-xs-4 none buy_img phone"><a><img src={host+order.item_image} /></a></div>
                         <div  className="col-md-10 col-sm-8 col-xs-8 none phone">
                            <div className="col-xs-12 none">
                               <div className="col-xs-9 none z_black limit f7">{order.name}</div>
                            </div>
                            <div className="col-xs-12 none">
                               <div className="pull-left limt f8">{order.description}</div>
                            </div>
                            {order.variant_type=='P' ? (
                                            
                                            <div className="col-xs-12 none" ng-if="order.variant_type=='P'">
                                                  <div ng-if='!editpage' className="pull-left limt"><b>{"package"}</b> : {order.variation_name}</div>
                                                
                                            </div>
                                          ) : ''}
                            { order.attributes.length > 0 ? (<Attributes data={order.attributes} />) : null}
                            
                           
                            <div className="col-xs-12 none">
                               <div className="col-xs-5 none limit text_1 f8">{config.toFixed(order.payed_price/order.quantity_ordered) + config.currency}</div>
                               <div ng-if='!editpage' className="col-xs-2 none f8">{"Qty :"+ order.quantity_ordered}</div>
                               
                               <div className="col-xs-5 none text-right limit f7">{order.payed_price + config.currency}</div>
                            </div>
                         </div>
                      </div>
                      )
                })}
              
                    </div>           
                 </div>

                <div className="col-md-6 col-sm-6 col-xs-12 padding_md pull-right f8">
                   <div  className="col-md-8 col-sm-6 col-xs-8 text-right none">
                      <div>Subtotal:</div>
                      <div>Shipping:</div>
                      <div>Taxes:</div>
                      {orderdata.coupon_discount ? (
                        <div >Discount:</div>
                        ) : ''}
                      <div className="z_black">Total:</div>
                   </div>
                   
                   <div className="col-md-4 col-sm-6 col-xs-4 text-right none">
                      <div><b>{config.toFixed(orderdata.total) + config.currency}</b></div>
                      <div>{config.toFixed(orderdata.shipping_cost_total) + config.currency}</div>
                      <div>{config.toFixed(orderdata.taxes) + config.currency}</div>
                      {orderdata.coupon_discount ? (
                        <div >{ config.toFixed(orderdata.coupon_discount) + config.currency }</div>
                        ) : ''}

                      <div  className="z_black"><b>{config.toFixed(orderdata.total + orderdata.taxes + orderdata.shipping_cost_total - config.toFixed(orderdata.coupon_discount)) + config.currency}</b></div>
                   </div>
                </div>
             </div>
        </div>
      ) : null}
      </form>
    )
  }
}

class Attributes extends Component { 

  render() {  
    var attributes = this.props.data.map(function(attribute){      
        return (
              
               <div className="col-xs-12 none" >
                  <div className="pull-left limt"><b>{attribute.name}</b> : {attribute.value}</div>
               </div>   
        )        
    })
    return (
      {attributes} 
      )
  }
}