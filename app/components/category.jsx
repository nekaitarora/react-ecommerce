import React, { Component } from 'react';
import { Link } from 'react-router';
import Carousel from './carousel.jsx';
import config from "../common/config.jsx";
import API from "../common/api.jsx";
import Products from "./products.jsx";

let baseAPIUrl = config.baseApiUrl;

var scroll_in_process = false;
var lastScrollTop = 0;
var skip = 6;
var limit = 6;
var listener;


export default class Category extends Component {
  constructor(props) {
    super(props);
    this.state = {
      storeCategory: this.props.storeCategory,
      parent_category_code: null,
      categoryReq: null,
      parent_category: null,
      showCategory: null,
      productArray: []
    };
  }

  componentWillReceiveProps(newProps) {
    var parent_category_code = newProps.params.category_code;
    var categoryReq = this.props.location.query.category_code;
      this.setState({
        parent_category_code: parent_category_code,
        categoryReq : categoryReq
      }, function() {
        this.getCategoryProducts();
      });
  }

  componentWillMount() {
    this.setState({
      parent_category_code: this.props.params.category_code
      }, function() {
        listener = this.handleScroll.bind(this)
        window.addEventListener('scroll', listener);
        this.getCategoryProducts();
      });
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', listener);
  }

  getCategoryProducts() {
    var self = this;
    var storeCategory = this.state.storeCategory;
    var parent_category_code = this.state.parent_category_code;
    var categoryReq = {
      name : this.props.location.query.category_name,
      categoryCode : this.props.location.query.category_code
    };
    var showCategory = {};

    if(storeCategory && parent_category_code) {
      var parent_category = [];
      var showCategory = {};

      parent_category = storeCategory.filter(function(obj) {
        return obj.category_code == parent_category_code;
      });

      if(categoryReq.name && categoryReq.categoryCode) {
        showCategory = {
          name : categoryReq.name,
          categoryCode: categoryReq.categoryCode
        };
      } else {
        showCategory = {
          name : parent_category[0].name,
          categoryCode: parent_category_code
        };
      }

      this.setState({
        categoryReq : categoryReq.categoryCode,
        parent_category: parent_category[0],
        showCategory: showCategory,
        productArray: []
      });

      API.get(baseAPIUrl + "product/category/"+showCategory.categoryCode+"/?skip=0&limit=6", null, function(err,data,more) {
        if(data.length != 0) {
          self.setState({
            productArray : data
          });
        }
      });
      scroll_in_process = false;
      lastScrollTop = 0;
      skip = 6;
      limit = 6;
    }
  }

  handleScroll(event) {
    var self = this;
    var showCategory = this.state.showCategory;
    if(showCategory && showCategory.categoryCode) {
      var category_code = showCategory.categoryCode;
      var st = $(window).scrollTop();
      if (st > lastScrollTop) {
        if (!scroll_in_process && $("#scrollDiv").offset() && (st + $(window).height()) > $("#scrollDiv").offset().top) {
          scroll_in_process = true;
          API.get(baseAPIUrl + "product/category/"+category_code+"/?skip="+skip+"&limit="+limit, null, function(err,data,more) {
            if(data.length == 0) {
              console.log("Data End!!");
            } else {
              var productArray = self.state.productArray;
              for(var i = 0; i < data.length; i++) {
                productArray.push(data[i]);
              }
              self.setState({
                productArray : productArray
              })
              skip = skip + limit;
              scroll_in_process = false;
            }
          }); 
        }
      }
      lastScrollTop = st;
    }
  }

  filterProducts(name, categoryCode) {
    var self = this;
    var showCategory = {
      name :name,
      categoryCode: categoryCode
    };

    this.setState({
      showCategory: showCategory,
      productArray: []
    });

    API.get(baseAPIUrl + "product/category/"+showCategory.categoryCode+"/?skip=0&limit=6", null, function(err,data,more) {
      if(data.length != 0) {
        self.setState({
          productArray : data
        });
      }
    });
    scroll_in_process = false;
    lastScrollTop = 0;
    skip = 6;
    limit = 6;
  }

  render() {
    var self = this;
    var host = config.baseApiUrl;
    var parent_category = this.state.parent_category;
    var showCategory = this.state.showCategory;
    var productArray = this.state.productArray;
    var categoryHeader = null;
    var allSelectedOption = null;
    var categoryList = null;
    var products = null;
    if(!(parent_category && showCategory)) {
      return (
        <div>
          <h1>Loading...</h1>
        </div>
      );
    } else {

      categoryHeader = (
        <span>{(parent_category.name).toUpperCase()}</span>
      );

      if(parent_category.category_code === showCategory.categoryCode) {
        allSelectedOption = ( <li><a className={"menu_select filterActive"}><b>ALL</b></a></li> );
      } else {
        allSelectedOption = ( <li>
          <Link to={"category/" + parent_category.category_code} className={"menu_select clickAction"}>
            <b>ALL</b>
          </Link>
        </li> );
      }

      categoryList = parent_category.sub_categories.map(function(sub_category, index) {
        if(sub_category) {
          if(sub_category.category_code === showCategory.categoryCode) {
            return (
              <li key={index} >
                <a className={"menu_select filterActive"}><b>{(sub_category.name).toUpperCase()}</b></a>
              </li>   
            );
          } else {
            return (
              <li key={index} >
                <Link key={index} to={"category/" + parent_category.category_code + "?category_code=" + sub_category.category_code + "&category_name=" + sub_category.name} className="menu_select clickAction">
                  <b>{(sub_category.name).toUpperCase()}</b>
                </Link>
              </li> 
            );
          }
        }
      });

      return (
        <div className="col-xs-12 none" id="category">
          <section>
            <div className="col-xs-12">
                <div className="col-xs-12 f5 padding_lg">
                  {categoryHeader}
                </div>
                <Carousel imageArray = {parent_category && Object.keys(parent_category).length ? parent_category.images : []}/>
                <div className="col-xs-12 padding_lg">
                  <nav className="navbar cate_menu navbar-default navi bnone" role="navigation">
                    <div className="container-fluid none">
                      <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
                          <span className="sr-only">Toggle Navigation</span>
                          <span className="icon-bar"></span>
                          <span className="icon-bar"></span>
                          <span className="icon-bar"></span>
                        </button>
                       </div>
                      <div className="navbar-collapse collapse small-nav menu margin none" id="bs-example-navbar-collapse-2">
                        <ul className="nav navbar-nav none f10">
                          {allSelectedOption}
                          {categoryList}
                        </ul>
                      </div>
                    </div>
                  </nav>
                </div>
            </div>
          </section>
          {this.state.productArray && this.state.productArray.length ? (
            <Products products={this.state.productArray} name="categoryProducts" />
          ) : (
            <div className="col-xs-12 padding_lg margin-top-3">
              <div className="col-xs-12 f5 text-center alert alert-warning">
                <span>No products in this category.</span>
              </div>
            </div>
          )}
          <div id="scrollDiv">&nbsp;</div>
        </div>
      );
    }
  }
}