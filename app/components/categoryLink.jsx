import React, { Component } from 'react';
import { Link } from 'react-router';

export default class CategoryLink extends Component {
  
  enterTest(event) {
    var left = $(event.target).offset().left;
    var width = $(event.target).width() + 30;
    var cssObj = {
      'width' : width + 'px',
      'left' : left + 'px'
    };
    cssObj.transition = 'width 1.2s, left .8s',
    $('.animate-border').css({'width':'0px'});
    $('.animate-border').css(cssObj).delay(800);
  }
  
  exitTest(event) {
    $('.animate-border').css({'width':'0px', 'left':'0px'});
  }

  render() {
    var storeCategory = this.props.storeCategory ? this.props.storeCategory : null;

    if(storeCategory && storeCategory.length) {
      var categories = [];
      for(var i=0; i < storeCategory.length; i++) {
        var category = storeCategory[i];
        if(category) {
          categories.push(
            (<li onMouseEnter={(event) => {this.enterTest(event)}} onMouseLeave={(event) => {this.exitTest(event)}} key={i}><Link to={"category/" + category.category_code} className="menu_select" ><b>{(category.name).toUpperCase()}</b></Link></li>)
          );
        } else {
          break;
        }
      }
    }

    return (
      <div className="col-md-12 navbar-collapse collapse in small-nav menu margin none categoriesLink" id="bs-example-navbar-collapse-1">
        <ul className=" visible-lg visible-md nav navbar-nav f11 text-align-left">
            {categories}
            <li className="animate-border"></li>
        </ul>
      </div>
    );
  }
}