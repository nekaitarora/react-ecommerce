import React, { Component } from 'react';
import { Link } from 'react-router';
export default class Other extends Component {
  render() {
     console.log("other",this.props);
    return (
      <div>
        <Link to="/">Home</Link><br/>
        <Link to="other">Other</Link><br/>
        <Link to="settings">Settings</Link>
        <h1>This is other component.</h1>
      </div>
    )
  }
}