import React, { Component } from 'react';
import { Link } from 'react-router';

export default class NavMenu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      parentCategory : null,
      subCategory : null
    };
  }

  showSubCat(category) {
    if(category) {
      this.setState({
        parentCategory: category,
        subCategory: category.sub_categories
      });
    }
    var cssObj = {
      "width" : "200px"
    };
    cssObj.transition = 'width .5s',
    $('.nav-subCat-div').css(cssObj);
  }
  
  hideSubCat() {
    var cssObj = {
      "width" : "0px"
    };
    $('.nav-subCat-div').css(cssObj);
  }

  hideMenu() {
    this.props.onCloseMenu();
  }

  showMenu() {
    this.props.onOpenMenu();
  }

  render() {
    var self = this;
    var storeCategory = this.props.storeCategory ? this.props.storeCategory : null;
    var parentCategory = this.state.parentCategory;
    var subCategory = this.state.subCategory;
    var parentCategoryComp = null;
    var showParentCategoryLink = null;
    var subCategoryComp = null;

    if(!(storeCategory && storeCategory.length)) {
      return null;
    } else {
      parentCategoryComp = storeCategory.map(function(category, index) {
        return (
          <span key={index} className="nav-list-span nav-item" onMouseEnter={() => self.showSubCat(category)} onMouseLeave={() => self.hideSubCat()} >
            <Link to={"category/" + category.category_code} >
              <span className="nav-text">{category.name}<span className="pull-right glyphicon glyphicon-chevron-right"></span></span>
            </Link>
          </span>
        );
      });

      if(subCategory && subCategory.length) {
        showParentCategoryLink = (
          <span className="nav-text">{parentCategory.name}</span>
        );

        subCategoryComp = subCategory.map(function(category, index) {
          return (
            <Link key={index} to={"category/" + parentCategory.category_code + "?category_code=" + category.category_code + "&category_name=" + category.name} >
              <span className="nav-text">{category.name}</span>
            </Link>
          );
        });
      }

      return (
        <div className="nav-menu" onMouseEnter={() => this.showMenu()} onMouseLeave={() => this.hideMenu()}>
          <div className="nav-arrow">
            <div className="nav-arrow-inner"></div>
          </div>
          <div className="nav-parent-div">
            {parentCategoryComp}
          </div>
          <div className="nav-subCat-div" onMouseEnter={() => this.showSubCat(false)} onMouseLeave={() => this.hideSubCat()} >
            <div className="nav-column">
              <span className="nav-title nav-item">
                {showParentCategoryLink}
              </span>
              <div className="nav-panel">
                {subCategoryComp}
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}