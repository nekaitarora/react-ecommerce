import React, { Component } from 'react';

export default class About extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      store : this.props.storeData
    };
  }

  render() {
    var store = this.state.store;
    if(!store) {
      return null;
    } else {
      return (
        <div>
          <section>
            <div className="col-xs-12 none margin_lg">
              <div className="col-xs-12 text-left f2 padding-bottom-10"><h1><b>{"ABOUT " + (store.name).toUpperCase()}</b></h1></div>
              <div className="col-xs-12">
                <p>{store.name + " is a young and vibrant company that aims to provide good quality branded products. " + store.name + " caters to the fashion needs of men, women and kids across footwear, apparel, jewellery, accessories and home decore."}</p>
                <p>{"At " + store.name + ", we strive to achieve the highest level of “Customer Satisfaction” possible. Our cutting edge E-commerce platform, highly experienced buying team, agile warehouse systems and state of the art customer care centre provides customer with:"}</p>
                <ul>
                  <li>Broader selection of products</li>
                  <li>Superior buying experience</li>
                  <li>On-time delivery of products</li>
                  <li>Quick resolution of any concerns</li>
                </ul>
              </div>
            </div> 
          </section>          
         
        </div>
      );
    }
  }
}
