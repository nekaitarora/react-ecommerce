import React, { Component } from 'react';
import config from "../common/config.jsx";
import API from "../common/api.jsx";
import store from '../store.jsx';
import { setUser } from '../actions/user.jsx';

let baseAPIUrl = config.baseApiUrl;
export default class DialogBox extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showLoginDialog : true,
      emailid : null,
      password : null,
      confirmPassword : null,
      term : false,
      errorMessage : null,
      login_emailid : null,
      login_password : null
    };
  }
  
  toggleDialog() {
    
    this.setState({
      emailid : null,
      password : null,
      confirmPassword : null,
      term : false,
      errorMessage : null,
      login_emailid : null,
      login_password : null
    });

    if(this.state.showLoginDialog) {
      this.setState({
        showLoginDialog : false
      });
    } else {
      this.setState({
        showLoginDialog : true
      });
    }
  }

  hideDialogBox() {
    this.props.onOpenCloseDialog(false);
  }

  userLogged() {
    this.props.onUserLogged();
  }

  setStateValue(event, key) {
    var value = event.target.value;
    var changeObj = {};
    changeObj[key] = value;
    this.setState(changeObj);
  }

  login() {
    var that = this;
    var login_emailid = this.state.login_emailid;
    var login_password = this.state.login_password;
    var emailChecker = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    if (!(login_emailid && login_password)) {
      this.setState({errorMessage : "All fields are mandatory."});
    } else {
      if(!emailChecker.test(login_emailid)){
        this.setState({errorMessage : "Email-Id is invalid."});
      } else {
        this.setState({errorMessage : null});
        var user = {
          "email": login_emailid,
          "password": login_password
        };
        API.post(baseAPIUrl + 'login', user, (err, result, more) => {
          if(result) {
            if(result == "error") {
              this.setState({errorMessage : "Email-Id and password didn't matched."});
            } else {
              var cookiesdata;
              if (result.first_name && result.last_name) { 
                cookiesdata = {'first_name':result.first_name,'last_name':result.last_name,"ismalladmin":result.ismalladmin};  
              } else {
                cookiesdata = {'username':result.username,"ismalladmin":result.ismalladmin};
              }
              store.dispatch(setUser(JSON.stringify(cookiesdata)));
              that.userLogged();
              that.hideDialogBox();
            }
          } else {
            console.log("Err:", err);
          }
        });
      }
    }
  }

  registration() {
    var that = this;
    var emailid = this.state.emailid;
    var password = this.state.password;
    var confirmPassword = this.state.confirmPassword;
    var term = this.state.term;
    var emailChecker = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    var passChecker = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
    if(!(emailid && password && confirmPassword)) {
      this.setState({errorMessage : "All fields are mandatory."});
    } else {
      if(!emailChecker.test(emailid)){
        this.setState({errorMessage : "Email-Id is invalid."});
      } else {
        if(!passChecker.test(password)){
          this.setState({errorMessage : "Password is too easy."});
        } else {
          if(password != confirmPassword) {
            this.setState({errorMessage : "Password didn't matched."});
          } else {
            if(!term){
              this.setState({errorMessage : "Terms and conditions are not accepted."});
            } else {
              this.setState({errorMessage : null});
              var user = {
                "username": emailid,
                "password": password,
                "confirm_password": confirmPassword,
                "email": emailid
              };
              API.post(baseAPIUrl + 'signup', user, (err, result, more) => {
                if(result) {
                  that.toggleDialog();
                } else {
                  console.log("Err:", err);
                }
              });
            }
          }
        }
      }
    }
  }

  render() {
    var client = config.clientUrl; 
    var location = this.props.location.pathname; 
    var showLoginDialog = this.state.showLoginDialog;
    var loginSignupDailog = null;

    if(showLoginDialog) {
      loginSignupDailog = (
        <div className="popup_2" id="login_popup">
          <div className="bg_2 col-xs-12 none">
            <div className="col-xs-12 bg_1">
              <div className="padding_lg f9">
                <b>Sign in to Daffodil Store</b>
              </div>
            </div>
            <div className="col-xs-12 padding_md text-center text_1 "></div>

            <div className="padding_lg col-xs-12 border_b">
              <form name="login">
                <div className="col-xs-12 padding_sm">
                    {this.state.errorMessage}
                </div>
                <div className="col-xs-12 padding_sm">
                  <i className="glyphicon glyphicon-user form-i"></i>
                  <input type="email"  name="Username" placeholder="Username" required="" onChange={(event) => {this.setStateValue(event, 'login_emailid')}}/>
                </div>
                <div className="col-xs-12 padding_sm">
                  <i className="glyphicon glyphicon-lock form-i"></i>
                  <input name="password" placeholder="Password" type="password" required="" onChange={(event) => {this.setStateValue(event, 'login_password')}}/>
                </div>
                <div className="col-xs-12 error "></div>
                <div className="col-xs-12 text-center padding_sm">
                  <button className="login_button text_2 " type="button" onClick={() => {this.login()}}>Sign In</button>
                </div>
              </form>
            </div>

            <div className="text-center col-xs-12 padding_lg border_b">
              <div id="facebook" className="padding_sm">
                <a href={baseAPIUrl + "auth/facebook/login?success_redirect=" + client + location + "&failure_redirect=" + client}>
                  <div className="socialButtonLogin fb log_socialbtn text_2 phone ">
                    Sign In with facebook
                  </div>
                </a>
              </div>
              <div id="google" className="padding_sm">
                <a href={baseAPIUrl + "auth/google/login?success_redirect=" + client + location + "&failure_redirect=" + client}>
                  <div className="socialButtonLogin gp log_socialbtn text_2 phone "> 
                    Sign In with google
                  </div>
                </a>
              </div>
              <div id="twitter" className="padding_sm">
                <a href={baseAPIUrl + "auth/twitter/login?success_redirect=" + client + location + "&failure_redirect=" + client}>
                  <div className="socialButtonLogin tw log_socialbtn text_2 phone ">
                    Sign In with twitter
                  </div>
                </a>
              </div>
            </div>  

            <div className="text-center col-xs-12 padding_lg">
              <div >
                <a href="" className="">Forgot Password ?</a>
              </div>
            <div id="signup" className="">
              Dont have an account ? <a onClick={() => {this.toggleDialog()}} >signup</a>
            </div>
            
            </div>

          </div>
          <div className="ngdialog-close" onClick={() => {this.hideDialogBox()}}></div>
        </div>
      )
    } else {
      loginSignupDailog = (
        <div className="popup_2" id="registration">
          <div className="bg_2 col-xs-12 none">
            <div className="col-xs-12 bg_1">
              <div className="padding_lg f9">
                <b>Registration</b>
              </div>
            </div>
            
            <div className="padding_lg col-xs-12 border_b">
              <form name="registration">
                <div className="col-xs-12 padding_sm">
                    {this.state.errorMessage}
                </div>

                <div className="col-xs-12 padding_sm">
                  <i className="glyphicon glyphicon-envelope form-i"></i>
                    <input type="email" name="Email" placeholder="Email ID" required="" onChange={(event) => {this.setStateValue(event, 'emailid')}}/>
                </div>

                <div className="col-xs-12 padding_sm">
                  <i className="glyphicon glyphicon-lock form-i"></i>
                  <input name="password" placeholder="Password" type="password" required="" onChange={(event) => {this.setStateValue(event, 'password')}}/>
                </div>
                  
                <div className="col-xs-12 padding_sm">
                  <i className="glyphicon glyphicon-lock form-i"></i>
                    <input type="password" placeholder="Confirm Password" name="confirmPassword" required="" onChange={(event) => {this.setStateValue(event, 'confirmPassword')}}/>
                </div>

                <div className="col-xs-12 padding_sm ng-binding">
                  <input type="checkbox" name="Terms" required="" onChange={(event) => {this.setStateValue(event, 'term')}} />Accept Terms and Conditions <br/>
                </div>

                <div className="col-xs-12 text-center padding_sm">
                  <button className="login_button text_2" type="button" onClick={() => {this.registration()}}>
                    <strong>Register</strong>
                  </button>
                </div>
              </form>
            </div>

            <div className="text-center col-xs-12 padding_lg border_b">
              <div id="facebook" className="padding_sm">
                <a href={baseAPIUrl + "auth/facebook/signup?success_redirect=" + client + location + "&failure_redirect=" + client}>
                  <div className="socialButtonLogin fb log_socialbtn text_2 phone">Continue with facebook</div>
                </a>
              </div>

              <div id="google" className="padding_sm">
                <a href={baseAPIUrl + "auth/google/signup?success_redirect=" + client + location + "&failure_redirect=" + client}>
                  <div className="socialButtonLogin gp log_socialbtn text_2 phone"> Continue with google</div> 
                </a>
              </div>

              <div id="twitter" className="padding_sm">
                <a href={baseAPIUrl + "auth/twitter/signup?success_redirect=" + client + location + "&failure_redirect=" + client}>
                  <div className="socialButtonLogin tw log_socialbtn text_2 phone">Continue with twitter</div>
                </a>
              </div>
            </div>  

            <div className="text-center col-xs-12 padding_lg" >
              Already have an Account? <a onClick={() => {this.toggleDialog()}} >Login</a>
            </div>
          </div>
          <div className="ngdialog-close" onClick={() => {this.hideDialogBox()}}></div>
        </div>
      )
    }

    return (
    	<div>
	      {this.props.showDialog ? (
	      	<div id="ngdialog8" className="ngdialog ngdialog-theme-plain ">
	        <div className="ngdialog-overlay"></div>
	        <div className="ngdialog-content">
	          {loginSignupDailog}
	        </div>
	      	</div>
	      ) : ''}
    	</div>
    )
  }
  
}