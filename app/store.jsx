import { compose, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'

import Reducers from './reducers/reducers.jsx'

const middleware = [ thunk ]

const store = createStore(
  Reducers,
  applyMiddleware(...middleware)
)

export default store;
