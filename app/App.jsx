import React, { Component } from 'react';
import { connect } from 'react-redux'
import _ from 'underscore';
import { Link } from 'react-router';

import Header from './header.jsx';
import Footer from './footer.jsx';
import Carousel from './components/carousel.jsx';
import ChatComponent from './containers/ChatComponent.jsx';
import * as cartActions from './actions/cart.jsx';
import * as homeActions from "./actions/home.jsx";


class App extends Component{

  constructor(props) {
    super(props);
    this.state = {
      storeCategory : null,
      cart: null,
      loggedUser: null,
      showDialog: false
    };
  }
  componentWillMount() {
    const { dispatch } = this.props;
    dispatch(homeActions.getStoreAPI());
    dispatch(homeActions.getCategoryAPI());
    console.log("dispatch action for cart data");
    dispatch(cartActions.getCartAPI());
  }
  componentWillReceiveProps(props) {   
    let self = this;
    const { state, user, cartReducer } = props;
    var subCategories = state.store && Object.keys(state.store) ? state.store.categories : [];
    var lookup = {};
    _.each(subCategories, function(data) { 
      if(data.parent_category) {
        if(!lookup[data.parent_category]) {
          lookup[data.parent_category] = {};
          lookup[data.parent_category].sub_categories = [];
        } 
        lookup[data.parent_category].sub_categories.push(data);
      }
      return true;
    });
    var allParentCategories = (state.categories && state.categories.length) ? _.indexBy(state.categories, function(data) { return data.category_code}) : {};
    if(allParentCategories && Object.keys(allParentCategories).length) {
      var storeCategory = [];
      var i = 0;
      for (var key in lookup) {
        if(!lookup.hasOwnProperty(key)) {
          continue;
        }
        storeCategory[i] = allParentCategories[key];
        storeCategory[i].sub_categories = lookup[key].sub_categories;
        i++;
      }
      self.setState({
        storeCategory: storeCategory
      });
    }
    var cart_data = cartReducer.cart;
    if(cart_data && Object.keys(cart_data).length) {
      self.setState({
        cart: cart_data
      });
    }
  }
  
  openCloseDialog(value) {
    this.setState({'showDialog' : value});
  }

  switchUser(user) {
    if(user) {
      this.setState({'loggedUser' : user});
    } else {
      this.setState({'loggedUser' : null});
    }
  }

  fetchCart() {
    var self = this;
    const { cartReducer } = this.props;
    var cart_data = cartReducer.cart;
    if(cart_data && Object.keys(cart_data).length) {
      self.setState({
        cart: cart_data
      });
    }
  }

  updateCart(item, cb) {
    var self = this;
    const { dispatch } = this.props;
    dispatch(cartActions.updateCartAPI(item));
    var itemImg = $('.myId').find('img').eq(0);
    self.flyToElement($(itemImg), $('.glyphicon-shopping-cart'));
  }

  flyToElement(flyer, flyingTo) {
    var $func = $(this);
    var divider = 10;
    var flyerClone = $(flyer).clone();
    $(flyerClone).css({width: $(flyer).width()/2, height:$(flyer).height()/2 ,position: 'absolute', top: $(flyer).offset().top + $(flyer).height()/4 + "px", left: $(flyer).offset().left + $(flyer).width()/4 +"px", opacity: 1, 'z-index': 1000});
    $('body').append($(flyerClone));
    var gotoX = $(flyingTo).offset().left + ($(flyingTo).width() / 2) - ($(flyer).width()/divider)/2;
    var gotoY = $(flyingTo).offset().top + ($(flyingTo).height() / 2) - ($(flyer).height()/divider)/2;
     
    $(flyerClone).animate({
        opacity: 0.4,
        left: gotoX,
        top: gotoY,
        width: $(flyer).width()/divider,
        height: $(flyer).height()/divider
    }, 800,
    function () {
        $(flyerClone).fadeOut('fast', function () {
          $(flyingTo).fadeOut('fast', function () {
            $(flyingTo).fadeIn('fast', function () {
              $(flyingTo).fadeOut('fast', function () {
                $(flyingTo).fadeIn('fast', function () {
                });
              });
            });
          });
        });
    });
  }
  render() {
    const { state } = this.props;
    var storeCategory = this.state.storeCategory;
    if(!(state.store && storeCategory && storeCategory.length)) {
      $('#loadingDiv').css({'display':'block'});
      return null;
    } else {
      $('#loadingDiv').css({'display':'none'});
      var category = storeCategory.map(function (category, index) {
        return (
          <li key={index}><Link to={"category/" + category.category_code} className="menu-item">{(category.name).toUpperCase()}</Link></li>
        );     
      });

      // <ChatComponent onOpenCloseDialog = {(event) => {this.openCloseDialog(event)}} />              
      return (
        <div>
          <Header storeData = {state.store} storeCategory = {this.state.storeCategory} showDialog = {this.state.showDialog} {...this.props} onFetchCart={(event) => {this.fetchCart(event)}} onSwitchUser = {(event) => {this.switchUser(event)}} onOpenCloseDialog = {(event) => {this.openCloseDialog(event)}} />
            {(this.props.location.pathname == "/") ? <Carousel imageArray = {state.store && Object.keys(state.store).length ? state.store.Assets.images : []} /> : null }
            <div className="container">
              <div className="content">
                <div>{React.cloneElement(this.props.children, {'updateCart': this.updateCart.bind(this),'fetchCart': this.fetchCart.bind(this), 'onOpenCloseDialog': this.openCloseDialog.bind(this), 'loggedUser':this.state.loggedUser,  'storeData':state.store , 'storeCategory':this.state.storeCategory})}</div>
              </div>
            </div>  
          <Footer />
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  let user = (state.userReducer && Object.keys(state.userReducer).length) ? state.userReducer.authUser : {};
  return {state: state.homeReducer, user: user, cartReducer: state.cartReducer}
}

App = connect(mapStateToProps)(App)

export default App;