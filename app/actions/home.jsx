import Redux from 'redux';

export const SAVE_STORE = 'SAVE_STORE'
export const SAVE_CATEGORIES = 'SAVE_CATEGORIES'
export const SAVE_TRENDINGPRODUCTS = 'SAVE_TRENDINGPRODUCTS'
export const SAVE_ARRIVALPRODUCTS = 'SAVE_ARRIVALPRODUCTS'
export const SAVE_SCROLLPRODUCTS = 'SAVE_SCROLLPRODUCTS'
export const GET_PRODUCTS = 'GET_PRODUCTS'
export const GET_LATLNG = 'GET_LATLNG'
export const GET_TRENDINGPRODUCTS = 'GET_TRENDINGPRODUCTS'
export const GET_ARRIVALPRODUCTS = 'GET_ARRIVALPRODUCTS'
export const GET_PARENTCATEGORIES = 'GET_PARENTCATEGORIES'

import API from '../common/api.jsx';
import config from "../common/config.jsx";
let baseAPIUrl = config.baseApiUrl;


export function saveStore(store) {
	return {type: SAVE_STORE, store}
}

export function saveCategories(categories) {
	return {type: SAVE_CATEGORIES, categories}
}
/*to specify the type property to inform about the data that should be sent to the store.*/
export function saveTrendingProducts(trendingProducts) {
	return {type: SAVE_TRENDINGPRODUCTS, trendingProducts}
}
/*to specify the type property to inform about the data that should be sent to the store.*/
export function saveArrivalProducts(arrivalProducts) {
	return {type: SAVE_ARRIVALPRODUCTS, arrivalProducts}
}
/*to specify the type property to inform about the product data that should be sent to the store, to be used while handling scroll.*/
export function saveScrollProducts(scrollProducts) {
	return {type: SAVE_SCROLLPRODUCTS, scrollProducts}
}

export function getProducts(filter) {
	return {type: GET_PRODUCTS, filter}
}

export function getLatLng(filter) {
	return {type: GET_LATLNG, filter}
}

export function getTrendingProducts(filter) {
	return {type: GET_TRENDINGPRODUCTS, filter}
}

export function getArrivalProducts(filter) {
	return {type: GET_ARRIVALPRODUCTS, filter}
}

export function getParentcategories(filter) {
	return {type: GET_PARENTCATEGORIES, filter}
}

export function getStoreAPI() {
  	return (dispatch) => {
	    API.get(baseAPIUrl + "store",null, (err, data, more) => {
	    	if(data) {
	    		dispatch(saveStore(data[0]));
	    	}
	    });
  	}
}

export function getCategoryAPI() {
  	return (dispatch) => {
	    API.get(baseAPIUrl + "categories",null, (err, data, more) => {
	    	if(data) {
	    		dispatch(saveCategories(data));
	    	}
	    });
  	}
}
/*to get the trending products data*/
export function getTrendingProductsAPI() {
  	return (dispatch) => {
	    API.get(baseAPIUrl + "products/trending?skip=3&limit=9",null, (err, data, more) => {
	    	if(data) {
	    		dispatch(saveTrendingProducts(data));
	    	}
	    });
  	}
}
/*to get the newly arrived products data*/
export function getArrivalProductsAPI() {
  	return (dispatch) => {
	    API.get(baseAPIUrl + "products/newArrivals?limit=3",null, (err, data, more) => {
	    	if(data) {
	    		dispatch(saveArrivalProducts(data));
	    	}
	    });
  	}
}
/*to get products data for handling scrolling for more products*/
export function getScrollProductsAPI(skip,limit) {
  	return (dispatch) => {
	    API.get(baseAPIUrl + "product/?skip="+skip+"&limit="+limit,null, (err, data, more) => {
	    	if(data) {
	    		dispatch(saveScrollProducts(data));
	    	}
	    });
  	}
}