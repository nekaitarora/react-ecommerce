import Redux from 'redux';

export const GET_PRODUCTS = 'GET_PRODUCTS'

export function getProducts(filter) {
	return {type: GET_PRODUCTS, filter}
}