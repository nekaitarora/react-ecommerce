/* eslint no-console: 0 */


const cookieParser = require('cookie-parser');
// const session = require('express-session')
// const passport = require('passport')
// const bodyParser = require('body-parser');
// const MongoStore = require('connect-mongo')(session)
// const routes = require("./index.js"); 
// const busboy = require('connect-busboy');
// const fs = require('fs');
// const mongoose = require('mongoose');

const path = require('path');
const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3000 : process.env.PORT;
const app = express();

app.use(express.static(__dirname + '/dist'));

  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: 'dist',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('*', function response(req, res) {
    console.log(__dirname, 'dist/index.html')
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'dist/index.html')));
    res.end();
  });

  // app.use(express.static(__dirname + '/dist'));
  // app.get('*', function response(req, res) {
  //   res.sendFile(path.join(__dirname, 'dist/index.html'));
  // });


// app.use(busboy());

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());
// app.use(busboy({immediate:false}));
// app.use(session({ secret: 'testsession' }));

// app.use(session({
//   secret: 'testsession',
//   store: new MongoStore({
//     "mongooseConnection": mongoose.connection,
//     "db": "myexpressdb",
//     "host": "localhost",
//     "port": "27017",
//     "collection": "mysessions",
//     "clear_interval": 3600,
//     "auto_reconnect": true
//   })
// }));

// app.use(passport.initialize());
// app.use(passport.session());
// app.use('/',routes);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    console.log("err",err)
    res.status(err.status || 500).send(err.message);
    // res.send('error', {
    //   message: err.message,
    //   error: err
    // });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  console.log("err",err);
  res.status(err.status || 500).send(err);
  // res.send("errr",err);
  // res.render('error', {
  //   message: err.message,
  //   error: {}
  // });
});

// app.use('/',express.static(__dirname));


app.listen(5400,function(){
  console.log("server running at port 5400.");
});
